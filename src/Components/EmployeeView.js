import React from 'react'
import { useState } from 'react';
import axios  from 'axios';
import {
    Button
} from "react-bootstrap";
import { Form, Col, Table, Jumbotron } from 'react-bootstrap';
import { Formik } from 'formik';
import { computeHeadingLevel } from '@testing-library/dom';


const EmployeeView = (props) => {
  
    
    var currentEmployees = [];
    if(props.employees){
        currentEmployees = props.employees;
    }

    const handleOnClick = (index) => {
          console.log("onEmployeeSelected");
          if(props.customOnClick && currentEmployees.length > 0){
             props.customOnClick(currentEmployees[index]);
          }
    }
    
    

    return (
        <div>
            {currentEmployees.length > 0?
            <Table striped bordered hover>
        <thead>
            <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Organization</th>
        </tr>
    </thead>
  <tbody>
  {
            currentEmployees.map((employee, notAnindex) => {
                return(<tr onClick={() => {handleOnClick(notAnindex)}}>
                    <td>{employee.id}</td>
                    <td>{employee.first_name}</td>
                    <td>{employee.last_name}</td>
                    <td>{employee.email}</td>
                    <td>{employee.organization}</td>
                  </tr>)
          })
        }
  </tbody>
</Table>
:
<Jumbotron>
    <h5 className="display-5">No employees found!</h5>
</Jumbotron>
}
</div>

    )
}

export default EmployeeView
