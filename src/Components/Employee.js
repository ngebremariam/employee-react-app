import React from 'react'
import { useState } from 'react';
import axios  from 'axios';
import {
    Button
} from "react-bootstrap";
import { Form, Col } from 'react-bootstrap';
import { Formik } from 'formik';



const Employee = (props) => {

    const onFieldChange = (event) => {
        if (event.target.type == "number")
        {
            props.selectedEmployee[event.target.name] = Number(event.target.value);
        }
        else{
            props.selectedEmployee[event.target.name] =  event.target.value;
        }
    }  
    const handleSubmit= () => {
        console.log("handleSubmit");
        if(props.selectedEmployee){
            console.log("current employee");
            console.log(props.selectedEmployee);
            axios.put('http://localhost:3000/api/v1/employee/'+props.selectedEmployee.id, props.selectedEmployee)
            .then(response => {
            console.log("Update response");
            console.log(response);
        });
      }
    } 
// This is the default employee object
    var currentemployee = props.selectedEmployee || {
        "id": -1,
        "first_name": "",
        "last_name": "",
        "email": "",
        "phone": "",
        "organization": "",
        "designation": "",
        "salary": 0,
        "status": 0,
        "is_deleted": 1,
        "created_at": "2020-07-18T19:30:30.000Z",
        "updated_at": "2021-08-04T06:01:12.000Z"
    }


    return (
        <div>
        {
             <Formik>
                <Form className="employee-css">
                    <div className="header">
                    <h3>Employee Form</h3>
                    </div>
                   
                    <Form.Row>
                        <Form.Group as={Col}>
                            <Form.Label>
                                    Employee First Name
                                </Form.Label>
                            <Form.Control
                                //ref={titleRef}
                                type="text"
                                placeholder="Enter First Name"
                                name="first_name"
                                defaultValue={currentemployee.first_name}
                                onChange={onFieldChange}
                                required
                            />
                           
                        </Form.Group>
                        <Form.Group as={Col}>
                        <Form.Label>
                                    Employee Last Name
                                </Form.Label>
                            <Form.Control
                                className="index-input"
                                placeholder="Enter Last Name"
                                required
                                type="text"
                               // ref={descriptionRef}
                                name="last_name"
                                defaultValue={currentemployee.last_name}
                                onChange={onFieldChange}
                            />
                            
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col}>
                        <Form.Label>
                                    Employee Email Address
                                </Form.Label>
                            <Form.Control
                                className="index-input"
                                placeholder="Enter Email"
                                required
                                //ref={youtubeLinkRef}
                                name="email"                                
                                onChange={onFieldChange}
                                defaultValue={currentemployee.email}
                            />
                           

                        </Form.Group>

                        <Form.Group as={Col}>  
                        <Form.Label>
                                    Employee Phone No
                                </Form.Label>                      
                            <Form.Control
                                className="index-input"
                                placeholder="Enter Phone"
                                required
                               // ref={iosAppStoreRef}
                                name="phone"
                                onChange={onFieldChange}
                                defaultValue={currentemployee.phone}
                            />
                        </Form.Group>

                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col}>
                        <Form.Label>
                                    Employee Salary
                                </Form.Label>
                            <Form.Control
                                className="index-input"
                                placeholder="Enter Salary"
                                required
                                //ref={androidAppStoreRef}
                                name="salary"
                                defaultValue={currentemployee.salary}
                                onChange={onFieldChange}
                            />
                            
                        </Form.Group>
                        <Form.Group as={Col}>
                        <Form.Label>
                                    Employee Organization
                                </Form.Label>
                            <Form.Control
                                
                                placeholder="Enter Organization"
                                required
                                //ref={androidAppStoreRef}
                                name="organization"
                                defaultValue={currentemployee.organization}
                                onChange={onFieldChange}
                            />
                            
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col}>
                        <Form.Label>
                                    Employee Designation
                                </Form.Label>
                            <Form.Control
                                className="index-input"
                                placeholder="Enter Designation"
                                required
                                //ref={androidAppStoreRef}
                                name="designation"
                                defaultValue={currentemployee.designation}
                                onChange={onFieldChange}
                            />
                            
                        </Form.Group>
                        <Form.Group as={Col}>
                        <Form.Label>
                                    Employee Status
                                </Form.Label>
                            <Form.Control
                                
                                placeholder="Enter Status"
                                required
                                //ref={androidAppStoreRef}
                                name="status"
                                defaultValue={currentemployee.status}
                                onChange={onFieldChange}
                            />
                            
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col}>
                        <Form.Label>
                                    Employee Created Date
                                </Form.Label>
                            <Form.Control
                                
                                placeholder="Enter Created Date"
                                required
                                //ref={androidAppStoreRef}
                                type="text"
                                name="created_at"
                                defaultValue={currentemployee.created_at}
                                onChange={onFieldChange}
                            />
                            
                        </Form.Group>
                        <Form.Group as={Col}>
                        <Form.Label>
                                    Employee Updated Date
                                </Form.Label>
                            <Form.Control
                                
                                placeholder="Enter Updated Date"
                                required
                                type="text"
                                //ref={androidAppStoreRef}
                                name="updated_at"
                                defaultValue={currentemployee.updated_at}
                                onChange={onFieldChange}
                            />
                            
                        </Form.Group>
                    </Form.Row>
                    <Button onClick={handleSubmit} variant="secondary" type="submit">
                        Update
                    </Button>
                </Form>
                </Formik>
            }
    </div>

    )
}

export default Employee
