import Employee from './Components/Employee';
import EmployeeView from './Components/EmployeeView';
import { Tabs, Tab, Col, Button, Form, Table } from 'react-bootstrap';
import { useState } from 'react';
import React from 'react'
import axios  from 'axios';
import { Formik } from 'formik';
import { computeHeadingLevel } from '@testing-library/dom';


function App() {

  const [key, setKey] = useState('View');
  const [employees,set_employees]=useState([]);
  const [employee,set_employee]=useState(null);
  
  const onEmployeeSelected = (employee) => {
        console.log("onEmployeeSelected");
        set_employee(employee);
        setKey("Add");
  }

  React.useEffect(() => {

    axios.get('http://localhost:3000/api/v1/employee')
      .then(response => {
        set_employees(response.data)
    });

    let id=4;
    axios.get('http://localhost:3000/api/v1/employee/'+id)
      .then(response => {
        // set_first_name(response.data.firstName);
        // set_last_name(response.data.lastName);
        // set_email(response.data.email);
        // set_phone(response.data.phone);
        // set_salary(response.data.salary);
        // set_organization(response.data.organization);
        // set_designation(response.data.designation);
        // set_status(response.data.status);
        // let cDate=response.data.createdAt.split('T');
        // let uDate=response.data.createdAt.split('T');           
        // set_created_at(cDate[0]);
        // set_updated_at(uDate[0]);
        // console.log("Data");
        // console.log(response.data);
      });

  }, []);
  
  return (
    
    <div className="container">
      <Tabs
      id="controlled-tab-example"
      activeKey={key}
      onSelect={(k) => setKey(k)}
      className="mb-3"
    >
      <Tab eventKey="View" title="View all employees">
        <EmployeeView employees={employees} customOnClick={onEmployeeSelected}/>
        <EmployeeView employees={employees} customOnClick={(emp) => {console.log("Yo " + emp.first_name + "!")}}/>
        <EmployeeView/>
        <EmployeeView employees={employees}/>
        <EmployeeView employees={employees}/>
      </Tab>
      <Tab eventKey="Add" title="Add new employee">
        <Employee selectedEmployee={employee}/>
      </Tab>
    </Tabs>

    </div>
  );
}

export default App;
